# Paletra Blockchain: Do lero-lero a Solução


## Referências

- [UTXO vs Account](https://ethereum.stackexchange.com/questions/326/what-are-the-pros-and-cons-of-ethereum-balances-vs-utxos)
- [Multichain development](https://www.multichain.com/developers/)
- [Multichain Streams](https://www.multichain.com/developers/data-streams/)
- [Bitcoin Development Guide](https://bitcoin.org/en/developer-guide)
- [Livro Digital Gold: Bitcoin and the Inside Story of the Misfits and Millionaires Trying to Reinvent Money](https://www.amazon.com/gp/product/B00P6TZLOU)
- [Livro Mastering Bitcoin: Programming the Open Blockchain](https://www.amazon.com/Mastering-Bitcoin-Programming-Open-Blockchain-ebook/dp/B071K7FCD4)

## Outras blockchains interessantes

-[bigchaindb](https://www.bigchaindb.com/)
-[Hyperledger Projects](https://www.hyperledger.org/projects)
-[Corda](https://www.corda.net/)
-[Quorum](https://www.jpmorgan.com/global/Quorum)