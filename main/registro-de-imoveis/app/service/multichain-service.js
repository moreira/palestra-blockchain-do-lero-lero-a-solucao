String.prototype.fromHex = function() {
    var hex = this.toString();//force conversion
    var str = '';
    for (var i = 0; i < this.length; i += 2)
        str += String.fromCharCode(parseInt(this.substr(i, 2), 16));
    return str;
}

String.prototype.toHex = function() {
    var hex = '';
    for(var i=0;i<this.length;i++) {
        hex += ''+this.charCodeAt(i).toString(16);
    }
    return hex;
}

var multichain = null;

function getMultichain(){
    if(!multichain){
        multichain = require("multichain-node")({
            port: 8002,
            host: 'masternode',
            user: "multichainrpc",
            pass: "pass123"
        });
        
        multichain.getInfo((err, info) => {
            if(err){
                throw err;
            }
            console.log('running multichain client');
            console.log(info);
        });
    }
    return multichain;
}

exports.addPerson = function(person, okCallback, errorCallback){
    var data = JSON.stringify(person).toHex();
    getMultichain().publish({
        "stream":"registroPessoas",
        "key":person.id,
        "data":data
    }, (err, resp) => {
        if(err && errorCallback){
            errorCallback(err);
        }
        if(resp && okCallback){
            okCallback(resp);
        }
    })
}

exports.addHouse = function(house, okCallback, errorCallback){
    var data = JSON.stringify(house).toHex();
    getMultichain().publish({
        "stream":"registroImoveis",
        "key":house.id,
        "data":data
    }, (err, resp) => {
        if(err && errorCallback){
            errorCallback(err);
        }
        if(resp && okCallback){
            okCallback(resp);
        }
    })
}

exports.createStream = function(name, okCallback, errorCallback){
    getMultichain().create({
        "type":"stream",
        name,
        "open":true
    }, (err, resp) => {
        if(err && errorCallback){
            errorCallback(err);
        }
        if(resp){
            console.log('stream created, subscribing');
            multichain.subscribe({"stream":name}, (err2, resp2) => {
                if(err2 && errorCallback){
                    console.log('subscribing error');
                    errorCallback(err);
                }else if(resp2 && okCallback){
                    console.log('subcribing ok');
                    okCallback(resp);
                }else{
                    okCallback({"message":"ok"});
                }
            });
        }
    });
}

function listStream(stream, okCallback, errorCallback){
    getMultichain().listStreamKeys({stream}, (err,list) => {
        if(err && errorCallback){
            errorCallback(err);
        }

        if(list && okCallback){
            var resp = [];
            list.forEach(element => {
                resp.push({"key":element.key, "count":element.items});
            });
            okCallback(resp);
        }
    })
}

function loadKeyFromStream(stream, key, okCallback, errorCallback){
    getMultichain().listStreamKeyItems({stream, key}, (err, list) => {
        if(err && errorCallback){
            errorCallback(err);
        }
        if(list && okCallback){
            var resp = [];
            list.forEach(element => {
                var item = {key:element.key};
                item.date = element.blocktime;
                item.data = JSON.parse(element.data.fromHex());
                resp.push(item);
            });
            resp.sort( (e1, e2) =>{
                return e2.date - e1.date;
            } )
            okCallback(resp);
        }
    });
}

exports.listHouses = (okCallback, errorCallback) => {
    listStream("registroImoveis", okCallback, errorCallback);
}

exports.listPeople = (okCallback, errorCallback) => {
    listStream("registroPessoas", okCallback, errorCallback);
}

exports.loadHouse = (key, okCallback, errorCallback) => {
    loadKeyFromStream("registroImoveis", key, okCallback, errorCallback);
}

exports.loadPerson = (key, okCallback, errorCallback) => {
    loadKeyFromStream("registroPessoas", key, okCallback, errorCallback);
}

exports.updateHouse = (id, house, okCallback, errorCallback) => {
    var hex = JSON.stringify(house).toHex();
    getMultichain().publish({
        "stream":"registroImoveis",
        "key":id,
        "data":hex
    }, (err, resp) => {
        if(err && errorCallback){
            errorCallback(err);
        }
        if(resp && okCallback){
            okCallback(resp);
        }
    })
}