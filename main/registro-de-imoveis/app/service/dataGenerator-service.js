const uuid = require('uuid/v4');

Array.prototype.getRandomElement = function(){
    return this[Math.floor(Math.random()*this.length)];
}

const names = ['Jose', 'Joao', 'Fulano', 'Beltrano', 'Ciclano', 'Maria', 'Fulana', 'Beltrana', 'Ciclana'];
const surnames = ['Silva', 'Sousa', 'Santos', 'Olveira', 'Alves', 'Pereira'];
const cities = ['Brasilia/DF', 'Sao Paulo/SP', 'Rio de Janeiro/RJ', 'Fortaleza/CE', 'Natal/RN', 'Salvador/BA'];


function geraRandom(min, max) {
    return min + Math.round(Math.random()*max);
}

function mod(dividendo,divisor) {
    return Math.round(dividendo - (Math.floor(dividendo/divisor)*divisor));
}

//from https://gist.github.com/willianns/3246637
function cpf() {
    var n1 = geraRandom(0,9);
    var n2 = geraRandom(0,9);
    var n3 = geraRandom(0,9);
    var n4 = geraRandom(0,9);
    var n5 = geraRandom(0,9);
    var n6 = geraRandom(0,9);
    var n7 = geraRandom(0,9);
    var n8 = geraRandom(0,9);
    var n9 = geraRandom(0,9);
    var d1 = n9*2+n8*3+n7*4+n6*5+n5*6+n4*7+n3*8+n2*9+n1*10;
    d1 = 11 - (mod(d1,11));
    if (d1>=10) {
        d1 = 0;
    }
    var d2 = d1*2+n9*3+n8*4+n7*5+n6*6+n5*7+n4*8+n3*9+n2*10+n1*11;
    d2 = 11 - (mod(d2,11));
    if (d2>=10) {
        d2 = 0;
    }
    return ''+n1+n2+n3+n4+n5+n6+n7+n8+n9+d1+d2;
}

exports.generatePerson = function(){
    var name = names.getRandomElement() + " "+ surnames.getRandomElement();
    return {
        "id":uuid(),
        "name":name,
        "cpf":cpf()
    };
}

exports.generateHouse = function(){
    var address = "Rua "+names.getRandomElement() + " "+ surnames.getRandomElement();
    address+=", "+geraRandom(10,1000);
    address+=", "+cities.getRandomElement();
    return {
        "id":uuid(),
        address,
        "value":geraRandom(100000, 400000)
    };
}