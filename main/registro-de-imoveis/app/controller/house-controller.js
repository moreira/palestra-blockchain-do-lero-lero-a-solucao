const express = require('express');
const path = require('path');
const router = express.Router();
var multichainClient = require('../service/multichain-service');

router.get('/:id', (req, res)=> {
    multichainClient.loadHouse(req.params.id, (house) =>{
        res.send(house);
    }, error => {
        res.status(500).send(error);
    });
});

router.put('/:id', (req, res)=> {
    multichainClient.updateHouse(req.params.id, req.body, (ok)=>{
        res.status(200).send();
    }, (error)=>{
        res.status(500).send(error);
    })
});

module.exports = router