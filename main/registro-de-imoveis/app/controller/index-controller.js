const express = require('express');
const path = require('path');
const router = express.Router();
var multichainClient = require('../service/multichain-service');

// router.get('/', (req, res)=> {
//     res.sendFile('../web/index.html', {root: __dirname});
// });

router.get('/index/listPeople', (req,res) => {
    console.log('listing person');
    multichainClient.listPeople((list) =>{
        res.send(list);
    }, error => {
        res.status(500).send(error);
    });
});

router.get('/index/listHouses', (req,res) => {
    console.log('listing houses');
    multichainClient.listHouses((list) =>{
        res.send(list);
    }, error => {
        res.status(500).send(error);
    });
});

module.exports = router