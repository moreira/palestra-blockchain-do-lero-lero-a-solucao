const express = require('express');
const path = require('path');
const router = express.Router();
var multichainClient = false;
const dataGenerator = require('../service/dataGenerator-service');

function initMultichainClient(){
    if(!multichainClient){
        multichainClient = require('../service/multichain-service');
    }
}

router.post('/generatePerson', (req,res) => {
    console.log('generating person');
    initMultichainClient();
    multichainClient.addPerson(dataGenerator.generatePerson(), (ok) =>{
        res.send({"message":"Pessoa gerada"});
    }, (err) => {
        res.status(500).send(err);
    });
});
router.post('/generateHouse', (req,res) => {
    console.log('generating house');
    initMultichainClient();
    multichainClient.addHouse(dataGenerator.generateHouse(), (ok) =>{
        res.send({"message":"Imóvel gerado"});
    }, (err) => {
        res.status(500).send(err);
    });
});

router.post('/createStreams', (req,res) => {
    console.log('creating streams');
    initMultichainClient();
    multichainClient.createStream(req.body.streamName, (ok) =>{
        console.log(ok);
        res.send({"message":"streams "+req.body.streamName});
    }, (err) => {
        console.log(err);
        res.status(500).send(err);
    });    
});

module.exports = router