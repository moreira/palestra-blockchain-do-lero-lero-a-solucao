const express = require('express');
const path = require('path');
const router = express.Router();
var multichainClient = require('../service/multichain-service');

router.get('/:id', (req, res)=> {
    multichainClient.loadPerson(req.params.id, (person) =>{
        res.send(person);
    }, error => {
        console.log(error);
        res.status(500).send(error);
    });
});

module.exports = router