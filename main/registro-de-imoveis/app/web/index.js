function showOk(message){
  app.messageOk = message;
  setTimeout(function(){
    app.messageOk = false;
  }, 1500);
}

function showError(message){
  app.messageError = message;
  setTimeout(function(){
    app.messageError = false;
  }, 3000);
}

var app = new Vue({
    el:"#app",
    data:{
      messageOk:false,
      messageError:false,
      peopleList:[],
      houseList:[],
      currentPersonList:false,
      currentHouseList:false,
      selectedPerson:false,
    },
    methods:{
      clearData:function(){
        app.selectedPerson=false;
        app.currentPersonList = false;
        app.currentHouseList = false;
      },
      listPeople:function(event) {
        var that = this;
        this.$http.get('/index/listPeople')
          .then(list => {
            that.peopleList = list.body;
          })
          .catch(err => {
            showError(err);
          });
      },
      listHouse:function(event) {
        var that = this;
        this.$http.get('/index/listHouses')
          .then(list => {
            that.houseList = list.body;
          })
          .catch(err => {
            showError(err);
          });
      },
      loadHouse:function(id){
        var that = this;
        this.$http.get('/house/'+id).then(resp => {
          if(resp.ok){
            app.clearData();
            that.currentHouseList = resp.body;
            if(resp.body && resp.body[0] && resp.body[0].data && resp.body[0].data.dono){
              that.selectedPerson = resp.body[0].data.dono;
            }
          }else{
            showError(resp.body);
          }
        }).catch(err => {
          showError(err.body);
        });
      },
      loadPerson:function(id){
        var that = this;
        this.$http.get('/person/'+id).then(resp => {
            if(resp.ok){
              app.clearData();
              that.currentPersonList = resp.body;
            }else{
              showError(resp.body);
            }
          }).catch(err => {
            showError(err.body);
          });
      },
      saveHouseOwnerChange:function(){
        console.log(this.currentHouseList);
        var that = this;
        if(this.currentHouseList && this.currentHouseList[0] && this.currentHouseList[0].data){
          this.currentHouseList[0].data.dono = this.selectedPerson;
          this.$http.put('/house/'+this.currentHouseList[0].key, this.currentHouseList[0].data)
            .then( (res) => {
              showOk('Alterado com sucesso');
              app.listPeople();
              app.listHouse();
              app.clearData();
            }).catch( error => {
              showError('Não foi possível gravar');
            });
        }
      }
    }
});

app.listPeople();
app.listHouse();