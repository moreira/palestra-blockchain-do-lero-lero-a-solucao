function showOk(that, message){
  that.messageOk = message;
  setTimeout(function(){
    that.messageOk = false;
  }, 1500);
}

function showError(that, message){
  that.messageError = message;
  setTimeout(function(){
    that.messageError = false;
  }, 3000);
}

var app = new Vue({
    el:"#app",
    data:{
      messageOk:false,
      messageError:false
    },
    methods:{
      generatePerson:function(event) {
        var that = this;
        this.$http.post('/admin/generatePerson',{}).then(function(response){
          if(response.ok){
            showOk(that, response.body.message);  
          }else{
            showError(that, response.body);
          }
        }).catch(err => {
          showError(that, err.body);
        });
      },
      generateHouse:function(event) {
        var that = this;
        this.$http.post('/admin/generateHouse',{}).then(function(response){
          if(response.ok){
            showOk(that, response.body.message);  
          }else{
            showError(that, response.body);
          }
        }).catch(err => {
          showError(that, err.body);
        });
      },
      generateRegistroStream:function(streamName){
        var that = this;
        this.$http.post('/admin/createStreams',{streamName}).then(function(response){
          if(response.ok){
            showOk(that, response.body.message);  
          }else{
            showError(that, response.body);
          }
        }).catch(err => {
          showError(that, err.body);
        });
      }
    }
});