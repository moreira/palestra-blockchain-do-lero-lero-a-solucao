const express = require('express');
var bodyParser = require('body-parser')
const app = express();
const adminController = require('./app/controller/admin-controller');
const indexController = require('./app/controller/index-controller');
const houseController = require('./app/controller/house-controller');
const personController = require('./app/controller/person-controller');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static('app/web'));

app.use('/admin', adminController);
app.use('/house', houseController);
app.use('/person', personController);
app.use('/', indexController);


app.listen(3000, ()=>console.log('Start at port 3000 v1'));